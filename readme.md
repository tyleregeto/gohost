A simple virtual hosing app written in GO. Allows hosting of multi domains on a single IP address. This app is just a reverse proxy to other applications. Proxies are configured in a XML file.

This is a very minimal app, just hacked together quickly for my needs. I think the Gocode needs to be updated to 1.1 still...

Improvements are planned, I'll add them when the need arises or someone bugs me :)