package main

//IMPROVEMENTS
// - restart signals
// - error pages for down sites
// - email admin on down sites
// - log erros to external file

import (
	"encoding/xml"
	"fmt"
	"net/http"
	"net/http/httputil"
	"net/url"
	"os"
	"log"
)

var result = Result{Vhost: nil}
var basePath = ""

//var basePath = "../../"

type Vhost struct {
	Hostname   string
	Proxyto    string
	Redirectto    string
}

type Result struct {
	XMLName xml.Name "result"
	Vhost   []Vhost
}

func main() {
	log.Println("Running...")
	loadConfig(basePath + "./files/")
	
	for i := 0; i < len(result.Vhost); i++ {
		vhost := result.Vhost[i]
		url_, err := url.Parse(vhost.Proxyto)
		redirectUrl := vhost.Redirectto

		if err != nil {
			fmt.Println("err: " + err.Error())
		} else {
			if len(redirectUrl) > 0 {
				http.HandleFunc(vhost.Hostname, makeRedirectHandler(redirectUrl))
			} else {
				proxy := httputil.NewSingleHostReverseProxy(url_)
				http.HandleFunc(vhost.Hostname, makeHandler(proxy))
			}
		}
	}
	err := http.ListenAndServe(":80", nil)
	//log errors in external file
	if err != nil {
		log.Println("err: " + err.Error())
	}

	fmt.Println("Shutting down")
}

func makeHandler(proxy *httputil.ReverseProxy) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
	log.Print("proxyy it")
		proxy.ServeHTTP(w, r)
	}
}

func makeRedirectHandler(to string) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		http.Redirect(w, r, to, 301)
	}
}

func loadConfig(dir string) {
	file, err := os.Open(dir + "vhosts.xml")
	
	if err != nil {
		//TODO log the error
		log.Println("err: " + err.Error())
	} else {
		xml.NewDecoder(file).Decode(&result)
	}
}
